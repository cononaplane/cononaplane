from typing import Set
from django.contrib.auth.models import Group
from django.conf import settings
from django.db import models


from model_utils.models import TimeStampedModel as TSModel


class Department(TSModel):
    """
    We assume that a convention is organized by departments.

    In order to provide separate event logs for each department,
    each Event will be associated with one or more Departments via
    Many-to-Many relationship.

    A User may belong to one or more departments, and this will determine
    what they can see on the Event pages.
    """
    name = models.CharField(max_length=50, db_index=True)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)
    groups = models.ManyToManyField(Group, blank=True)

    has_log = models.BooleanField(default=False)

    @classmethod
    def for_user(cls, user) -> Set:
        """
        For a given user, find all the departments they should have access to whether by their
        own separate access or through their group.

        This uses subquery and set-union magic and somehow doesn't explode. We return only the
        PKs of departments we found, because really that's all a client of this requires, since it
        will probably be used as a further sub-query
        """
        user_match = Department.objects.filter(users=user)
        groups_match = Department.objects.filter(groups__id__in=user.groups.all().values('pk'))
        return {value['pk'] for value in user_match.union(groups_match).values('pk')}

    def __str__(self) -> str:
        return self.name
