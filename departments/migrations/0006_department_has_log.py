# Generated by Django 4.1.5 on 2023-02-26 00:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('departments', '0005_alter_department_users'),
    ]

    operations = [
        migrations.AddField(
            model_name='department',
            name='has_log',
            field=models.BooleanField(default=False),
        ),
    ]
