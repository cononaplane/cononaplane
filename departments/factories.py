import factory
import factory.django
import faker

from . import models

fake = faker.Faker()


class DepartmentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Department

    name = fake.name()
    has_log = True
