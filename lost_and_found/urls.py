from django.urls import path, re_path

from . import views

app_name = "lost_and_found"

urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("search/<str:status>", views.SearchFormView.as_view(), name="search"),
    path("results/<str:status>", views.SearchResultsView.as_view(), name="results"),
    path("inventory", views.InventoryView.as_view(), name="inventory"),
    path("detail/<pk>", views.ItemView.as_view(), name="detail"),
    path("new/<str:status>", views.NewItemView.as_view(), name="new"),
    path("edit/<pk>", views.UpdateItemView.as_view(), name="edit"),
]
