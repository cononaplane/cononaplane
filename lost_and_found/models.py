from django.conf import settings
from django.contrib.postgres.indexes import GinIndex
from django.db import models

from model_utils.models import TimeStampedModel as TSModel
from simple_history.models import HistoricalRecords

from base.models import Convention


class Category(TSModel):
    name = models.CharField(max_length=60, unique=True)
    in_use = models.BooleanField(default=True)

    def __str__(self) -> str:
        if self.in_use:
            return self.name
        else:
            return f"{self.name} (not in use)"


class Item(TSModel):
    STATUSES = {"found", "inventoried", "missing", "returned"}
    found = models.BooleanField(default=False, db_index=True)
    inventoried = models.BooleanField(default=False, db_index=True)
    missing = models.BooleanField(default=False, db_index=True)
    returned = models.BooleanField(default=False, db_index=True)

    where_found = models.CharField(max_length=200, null=True, blank=True)
    where_last_seen = models.CharField(max_length=200, null=True, blank=True)

    description = models.CharField(max_length=200)
    details = models.TextField()

    owner_contact = models.CharField(max_length=200, null=True, blank=True)
    owner_name = models.CharField(max_length=200, null=True, blank=True)
    who_claimed = models.CharField(max_length=200, null=True, blank=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)
    rolename = models.CharField(max_length=25)

    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    convention = models.ForeignKey(Convention, on_delete=models.DO_NOTHING)

    history = HistoricalRecords()

    @property
    def status(self) -> str:
        if self.returned:
            return "returned"
        if self.inventoried:
            return "inventoried"
        if self.found:
            return "found"
        if self.missing:
            return "missing"
        return "WTH"

    @status.setter
    def status(self, val: str) -> str:
        if val == "returned":
            self.returned = True
        if val == "inventoried":
            self.inventoried = True
        if val == "found":
            self.found = True
        if val == "missing":
            self.missing = True
        return val

    def clean(self) -> None:
        return super().clean()

    class Meta:
        indexes = [
            GinIndex(fields=['description', 'details'])
        ]
        ordering = ["created"]
        permissions = [(
            "can_inventory",
            "Can perform inventory functions"
        )]
