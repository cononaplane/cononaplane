import random

from django.contrib.auth.models import Group, Permission
from django.core.management.base import BaseCommand
from django.db import transaction

from base.models import Convention

from lost_and_found import factories as lfi_facts
from lost_and_found.models import Category


class Command(BaseCommand):
    help = "Not just anybody!"

    def handle(self, *args, **options) -> None:
        with transaction.atomic():
            self._load_fixtures()

    def _load_fixtures(self) -> None:
        ordinary_perms = [
            'view_item',
            "add_item",
            'change_item',
            "view_category"
        ]

        heads_perms = [
            "view_category",
            "add_category",
            "change_category"
        ]

        heads = Group.objects.get(name="Heads")
        for perm in Permission.objects.filter(codename__in=(ordinary_perms + heads_perms)):
            heads.permissions.add(perm)

        ordinaries = Group.objects.get(name="Ops Staff")
        for perm in Permission.objects.filter(codename__in=ordinary_perms):
            ordinaries.permissions.add(perm)

        CATEGORIES = (
            "Bags",
            "Bottles",
            "Clothing",
            "Electronics",
            "Glasses",
            "Headwear",
            "Jewelry",
            "Lockbox (Money, ID, Cards, Wallets, Keys, Badges)",
            "Papers",
            "Props",
            "Toys",
            "Other Not Listed"
        )

        for cat in CATEGORIES:
            lfi_facts.CategoryFactory.create(name=cat, in_use=True)

        for _ in range(10):
            lfi_facts.MissingItemFactory.create(category=Category.objects.get(name=random.choice(CATEGORIES)),
                                                convention=Convention.current)
            lfi_facts.FoundItemFactory.create(category=Category.objects.get(name=random.choice(CATEGORIES)),
                                              convention=Convention.current)
            lfi_facts.ReturnedItemFactory.create(category=Category.objects.get(name=random.choice(CATEGORIES)),
                                                 convention=Convention.current)
