from typing import Any, Dict, List, Type
from django.forms import CharField, CheckboxSelectMultiple, ChoiceField, Form, HiddenInput, IntegerField, \
    ModelChoiceField, ModelForm, ModelMultipleChoiceField, RadioSelect

from base.models import Convention
from .models import Category, Item


class SearchForm(Form):
    SEARCH_TYPES = (
        ("all", "Find All"),
        ("any", "Find Any"),
        ("phrase", "Find Exact Phrase"),
        ("websearch", "Advanced Query")
    )

    template_name = "lost_and_found/_search_form.html"

    convention = ModelChoiceField(queryset=Convention.objects.all())
    categories = ModelMultipleChoiceField(queryset=Category.objects.filter(in_use=True),
                                          widget=CheckboxSelectMultiple)
    keywords = CharField(max_length=200, required=False)
    search_type = ChoiceField(choices=SEARCH_TYPES,
                              widget=RadioSelect)

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.fields["convention"].initial = Convention.current


class ItemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ItemForm, self).__init__(*args, **kwargs)

    @property
    def missing(self) -> bool:
        return self.instance and self.instance.missing

    @property
    def found(self) -> bool:
        return self.instance and self.instance.found

    @property
    def returned(self) -> bool:
        return self.instance and self.instance.returned

    class Meta:
        model: Type = Item
        fields = [
            "category",
            "description",
            "where_last_seen",
            "owner_name",
            "owner_contact",
            "where_found",
            "who_claimed",
            "missing",
            "found",
            "returned",
            "inventoried"
        ]
        widgets: dict = {
            "missing": HiddenInput,
            "found": HiddenInput,
            "returned": HiddenInput,
            "inventoried": HiddenInput
        }

        @property
        def asdf(self) -> list:
            fields = ["category", "description"]
            if self.missing:
                fields += ["where_last_seen", "owner_name", "owner_contact"]
            if self.found:
                fields += ["where_found"]
            if self.returned:
                fields += ["who_claimed"]

            fields += ["details"]
            return fields


class JumpForm(Form):
    jump = IntegerField(min_value=1)

    def clean(self) -> Dict[str, Any]:
        cleaned_data = super().clean()
        pk = cleaned_data.get('jump')

        if pk:
            try:
                Item.objects.get(pk=pk)
            except Item.DoesNotExist:
                self.add_error('jump', 'No such item')
        return cleaned_data
