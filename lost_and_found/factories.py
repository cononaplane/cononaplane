import factory
import factory.django
import factory.fuzzy
import faker

from factory_djoy import CleanModelFactory
from accounts.factories import UserFactory

from base.factories import CurrentConventionFactory

from . import models

fake = faker.Faker(('la',))


class CategoryFactory(CleanModelFactory):
    class Meta:
        model = models.Category

    name = factory.fuzzy.FuzzyAttribute(lambda: fake.unique.word())
    in_use = True


class ItemFactory(CleanModelFactory):
    class Meta:
        model = models.Item

    found = False
    inventoried = False
    missing = False
    returned = False

    where_found = None
    where_last_seen = None

    description = factory.fuzzy.FuzzyAttribute(fake.sentence)
    details = factory.fuzzy.FuzzyAttribute(fake.paragraph)

    owner_contact = factory.fuzzy.FuzzyAttribute(fake.unique.phone_number)
    owner_name = factory.fuzzy.FuzzyAttribute(fake.unique.name)
    who_claimed = owner_name

    user = factory.SubFactory(UserFactory)
    rolename = fake.unique.word()

    category = factory.SubFactory(CategoryFactory)
    convention = factory.SubFactory(CurrentConventionFactory)


class MissingItemFactory(ItemFactory):
    missing = True
    where_last_seen = fake.unique.sentence()


class FoundItemFactory(ItemFactory):
    found = True
    where_found = fake.unique.sentence()


class ReturnedItemFactory(FoundItemFactory):
    missing = fake.boolean()
    returned = True
    where_last_seen = fake.unique.sentence() if missing else None
