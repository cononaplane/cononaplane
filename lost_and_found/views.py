from typing import Any, Dict, Type

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.postgres.search import SearchQuery, SearchVector
from django.db import transaction
from django.db.models import QuerySet
from django.forms.models import BaseModelForm
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, FormView, ListView, DetailView
from django.views.generic.edit import ModelFormMixin, CreateView, UpdateView

from base.models import Convention

from .forms import JumpForm, SearchForm
from .models import Item


class IndexView(PermissionRequiredMixin, TemplateView):
    """
    Implement the Lost and Found Splash Page, which begins the choose-your-adventure
    by presenting two paths: is the item being REPORTED MISSING, or REPORTED FOUND?

    This leads to the SearchFormView, always, never directly to creation.
    """
    template_name: str = "lost_and_found/index.html"
    permission_required = ("lost_and_found.view_item",)

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context: Dict[str, Any] = super().get_context_data(**kwargs)
        context["jump_form"] = JumpForm()
        self.request.session["inventory"] = False
        return context

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        if "jump" in request.GET:
            return redirect(reverse_lazy("lost_and_found:detail", args=(request.GET['jump'],)))
        return super().get(request, *args, **kwargs)


class SearchFormView(PermissionRequiredMixin, FormView):
    """
    By default, searches items reported missing or reported found by category, description, details.

    Always displayed before permitting a user to create a new item, to reduce the likelihood
    of duplicates.

    Colour coded for adventure path (missing vs found)
    """
    template_name: str = "lost_and_found/search.html"
    permission_required = ("lost_and_found.view_item",)
    form_class = SearchForm

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["body_bg"] = self.kwargs["status"]
        context["jump_form"] = JumpForm()
        return context

    def get_success_url(self) -> str:
        return reverse_lazy("lost_and_found:results", args=self.args, kwargs=self.kwargs)

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        with transaction.atomic():
            redir = self.get_success_url()
            qd = request.POST.copy()
            if "csrfmiddlewaretoken" in qd:
                qd.pop("csrfmiddlewaretoken")
            return redirect(redir + "?" + qd.urlencode())


class SearchResultsView(PermissionRequiredMixin, ListView):
    """
    Results of a search, natch.

    Also used to display the INVENTORY.
    """
    permission_required = ("lost_and_found.view_item",)
    model = Item
    paginate_by = 10

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["jump_form"] = JumpForm()
        if "status" in context:
            context["status"] = self.kwargs["status"]
            context["body_bg"] = context["status"]
            context["new_url"] = reverse("lost_and_found:new", kwargs={"status": context["status"]})
        context["jump_form"] = JumpForm()
        return context

    def get_queryset(self) -> QuerySet[Any]:
        q = Item.objects.filter(convention=self.request.GET.get('convention'))
        if "categories" in self.request.GET:
            q = q.filter(category__in=self.request.GET.getlist('categories') or [])
        if "keywords" in self.request.GET and self.request.GET.get("keywords") != "":
            vector = SearchVector('description', 'details')
            query = self.__format_search(self.request.GET.get("search_type"))
            q = q.annotate(search=vector) \
                 .filter(search=query)
        return q

    def __format_search(self, human_type: str | None) -> SearchQuery | None:
        if human_type is None:
            human_type = "any"

        words = self.request.GET.get("keywords", "")

        if human_type == "any":
            words = " OR ".join(words.split())
            search_type = "websearch"
        elif human_type == "all":
            search_type = "plain"
        elif human_type == "phrase":
            search_type = "phrase"
        else:
            search_type = "websearch"

        return SearchQuery(words, search_type=search_type)


class InventoryView(SearchResultsView):
    permission_required = ("lost_and_found.can_inventory",)
    model = Item
    paginate_by = 10

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["jump_form"] = JumpForm()
        context["body_bg"] = "inventoried"
        self.request.session["inventory"] = True
        context["inventory"] = self.request.session.get("inventory", False)
        return context

    def get_queryset(self) -> QuerySet[Any]:
        q = Item.objects.filter(convention=self.request.GET.get('convention') or Convention.current,
                                found=True, returned=False)
        return q



class ItemView(PermissionRequiredMixin, DetailView):
    """
    An individual LFI item, color-coded for status in the following priority order:
    missing (yellow)
    found (green)
    returned (blue)
    inventoried (purple)
    """
    permission_required = ("lost_and_found.view_item",)
    model = Item

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["jump_form"] = JumpForm()
        context["inventory"] = self.request.session.get("inventory", False)
        context["status"] = context["item"].status
        context["body_bg"] = context["status"]
        return context


class ItemFormView(PermissionRequiredMixin, ModelFormMixin):
    model = Item

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["jump_form"] = JumpForm()
        context["status"] = self.object.status if self.object is not None else self.kwargs["status"]
        context["body_bg"] = context["status"]
        return context

    def get_form(self, form_class: Type[BaseModelForm] | None = None) -> BaseModelForm:
        form = super().get_form(form_class)
        return form

    @property
    def fields(self) -> list:
        fields = ["category", "description", "found", "missing", "returned", "inventoried"]
        if self.missing:
            fields += ["where_last_seen", "owner_name", "owner_contact"]
        if self.found:
            fields += ["where_found"]
        if self.returned:
            fields += ["who_claimed"]

        fields += ["details"]
        return fields

    @property
    def missing(self) -> bool:
        return ("status" in self.kwargs and self.kwargs["status"] == "missing") or \
               (self.object and self.object.missing)

    @property
    def found(self) -> bool:
        return ("status" in self.kwargs and self.kwargs["status"] == "found") or \
               (self.object and self.object.found)

    @property
    def returned(self) -> bool:
        return ("status" in self.kwargs and self.kwargs["status"] == "returned") or \
               (self.object and self.object.returned)

    @property
    def inventoried(self) -> bool:
        return ("status" in self.kwargs and self.kwargs["status"] == "inventoried") or \
               (self.object and self.object.returned)


class NewItemView(ItemFormView, CreateView):
    """
    Create an item. Displayed fields depend on whether we're creating a MISSING or FOUND report.
    """
    permission_required = ("lost_and_found.add_item",)

    @property
    def success_url(self) -> str:
        return reverse_lazy("lost_and_found:detail", kwargs={"pk": self.object.pk})

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        if self.missing:
            context["form"].fields["missing"].initial = True
        if self.found:
            context["form"].fields["found"].initial = True

        return context

    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        form.instance.convention = Convention.current
        form.instance.user = self.request.user
        return super().form_valid(form)


class UpdateItemView(ItemFormView, UpdateView):
    """
    Update an item.

    Usually only happens to update status (a missing item was found; a found
    item was returned, or inventoried.).
    """
    permission_required = ("lost_and_found.change_item",)

    def __init__(self, **kwargs: Any) -> None:
        self.object = None
        super().__init__(**kwargs)

    @property
    def success_url(self) -> str:
        return reverse_lazy("lost_and_found:detail", kwargs={"pk": self.kwargs["pk"]})

    def get(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
        self.object = self.get_object()

        if "found" in request.GET:
            self.object.found = request.GET["found"]
        if "returned" in request.GET:
            self.object.returned = request.GET["returned"]
        if "inventoried" in request.GET:
            self.object.inventoried = request.GET["inventoried"]

        context = self.get_context_data()
        return self.render_to_response(context)
