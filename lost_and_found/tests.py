from abc import abstractmethod
import unittest
import urllib.parse
from django.http import HttpResponse, QueryDict
from django.test import TransactionTestCase
from django.urls import reverse

from accounts import factories as accounts
from base.factories import ConventionFactory, CurrentConventionFactory
from .factories import CategoryFactory, ItemFactory


# TODO: This is copypasta from log/tests.py; maybe abstract to base/tests.py?
class BaseTestCaseAuthenticated(TransactionTestCase):
    url = ""
    unauth_redirect = ""
    post_redirect = ""
    auth_render = None
    is_form = False

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        if cls is BaseTestCaseAuthenticated:
            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")

    @classmethod
    def tearDownClass(cls) -> None:
        return super().tearDownClass()

    def setUp(self) -> None:
        super().setUp()
        self.user = accounts.UserFactory.create()
        self.peon_user = accounts.UserFactory.create()
        self.group = accounts.HeadGroupFactory.create()
        self.convention = CurrentConventionFactory.create()
        self.category = CategoryFactory.create()
        self.other_category = CategoryFactory.create()
        self.user.groups.add(self.group)

    def test_get_unauthenticated(self) -> None:
        response = self.client.get(self.url)
        self.assertRedirects(response, self.unauth_redirect)

    def test_get_authenticated(self) -> None:
        self.client.force_login(self.user)
        response: HttpResponse = self.client.get(self.url)
        self.assertTemplateUsed(response, self.auth_render)

        self.do_more_get_authenticated(response)

    def test_post_authenticated(self) -> None:
        """
        Test the happy path of an authenticated POST

        This should always test a happy path, with get_form_data() returnning what
        a valid form would post. Individual test case classes will need to handle
        complexity in forms, because every form is different
        """
        if not self.is_form:
            return
        self.client.force_login(self.user)  # type: ignore
        form_data = self.get_form_data()
        response = self.client.post(self.url, data=form_data)
        self.assertRedirects(response, self.post_redirect)

    def test_post_authenticated_but_no_perms(self) -> None:
        if not self.is_form:
            return
        self.client.force_login(self.peon_user)  # type: ignore
        form_data = self.get_form_data()
        response = self.client.post(self.url, data=form_data)
        self.assertEqual(response.status_code, 403)

    @abstractmethod
    def do_more_get_authenticated(self, response: HttpResponse) -> None:
        pass

    @abstractmethod
    def get_form_data(self) -> dict:
        pass


class TestIndexView(BaseTestCaseAuthenticated):
    url = reverse("lost_and_found:index")
    unauth_redirect = f"{reverse('login')}?next={url}"
    auth_render = "lost_and_found/index.html"

    def do_more_get_authenticated(self, response: HttpResponse) -> None:
        pass

    def get_form_data(self) -> dict:
        return {}


class TestSearchFormViewMissing(BaseTestCaseAuthenticated):
    url = reverse("lost_and_found:search", kwargs={"status": "missing"})
    unauth_redirect = f"{reverse('login')}?next={url}"
    auth_render = "lost_and_found/search.html"
    is_form = True

    def get_form_data(self) -> dict:
        return {
            "categories": [self.category.pk, self.other_category.pk],
            "convention": self.convention.pk
        }

    def get_query_dict(self) -> QueryDict:
        qd = QueryDict(mutable=True)
        fd = self.get_form_data()
        qd.setlist("categories", fd["categories"])
        qd["convention"] = fd["convention"]
        return qd

    @property
    def post_redirect(self) -> str:
        redir = reverse('lost_and_found:results', kwargs={"status": "missing"})
        return redir + "?" + self.get_query_dict().urlencode()


class TestSearchResultsView(BaseTestCaseAuthenticated):
    auth_render = "lost_and_found/item_list.html"

    @property
    def url(self) -> str:
        qd = QueryDict(mutable=True)
        qd.setlist("categories", [self.category.pk])
        qd["convention"] = self.convention.pk
        return (reverse("lost_and_found:results", kwargs={"status": "missing"}) +
                "?" +
                qd.urlencode())

    @property
    def unauth_redirect(self) -> str:
        return f"{reverse('login')}?next={urllib.parse.quote_plus(self.url)}"

    def test_search_results_convention_category(self) -> None:
        self.client.force_login(self.user)

        find_me = ItemFactory.create(convention=self.convention,
                                     category=self.category)
        dont_find_me = ItemFactory.create(convention=self.convention,
                                          category=self.other_category)
        also_not_me = ItemFactory.create(convention=ConventionFactory.create(),
                                         category=self.category)

        response = self.client.get(self.url)
        self.assertIn(find_me, response.context["object_list"])
        self.assertNotIn(dont_find_me, response.context["object_list"])
        self.assertNotIn(also_not_me, response.context["object_list"])

    def test_search_results_keywords_any(self) -> None:
        self.client.force_login(self.user)

        find_me = ItemFactory.create(convention=self.convention,
                                     category=self.category,
                                     description="Wombat on toast")
        also_find_me = ItemFactory.create(convention=self.convention,
                                          category=self.category,
                                          details="Toast on wombat")
        find_me_three = ItemFactory.create(convention=self.convention,
                                           category=self.category,
                                           description="toast and butter")
        dont_find_me = ItemFactory.create(convention=self.convention,
                                          category=self.category)

        qd = QueryDict(mutable=True)
        qd.setlist("categories", [self.category.pk])
        qd["convention"] = self.convention.pk
        qd["keywords"] = "wombat toast"
        qd["search_type"] = "any"
        url = (reverse("lost_and_found:results", kwargs={"status": "missing"}) +
               "?" +
               qd.urlencode())
        response = self.client.get(url)

        self.assertQuerysetEqual([find_me, also_find_me, find_me_three], response.context["object_list"])
        self.assertNotIn(dont_find_me, response.context["object_list"])

    def test_search_results_keywords_all(self) -> None:
        self.client.force_login(self.user)

        find_me = ItemFactory.create(convention=self.convention,
                                     category=self.category,
                                     description="Wombat on toast")
        also_find_me = ItemFactory.create(convention=self.convention,
                                          category=self.category,
                                          details="Toast on wombat")
        dont_find_me = ItemFactory.create(convention=self.convention,
                                          category=self.category,
                                          description="toast and butter")
        dont_find_me_either = ItemFactory.create(convention=self.convention,
                                                 category=self.category)

        qd = QueryDict(mutable=True)
        qd.setlist("categories", [self.category.pk])
        qd["convention"] = self.convention.pk
        qd["keywords"] = "wombat toast"
        qd["search_type"] = "all"
        url = (reverse("lost_and_found:results", kwargs={"status": "missing"}) +
               "?" +
               qd.urlencode())
        response = self.client.get(url)

        self.assertQuerysetEqual([find_me, also_find_me], response.context["object_list"])
        self.assertNotIn(dont_find_me, response.context["object_list"])
        self.assertNotIn(dont_find_me_either, response.context["object_list"])

    def test_search_results_keywords_phrase(self) -> None:
        self.client.force_login(self.user)

        find_me = ItemFactory.create(convention=self.convention,
                                     category=self.category,
                                     description="Wombat on toast")
        dont_find_me = ItemFactory.create(convention=self.convention,
                                          category=self.category,
                                          details="Toast on wombat")
        qd = QueryDict(mutable=True)
        qd.setlist("categories", [self.category.pk])
        qd["convention"] = self.convention.pk
        qd["keywords"] = "wombat on toast"
        qd["search_type"] = "phrase"
        url = (reverse("lost_and_found:results", kwargs={"status": "missing"}) +
               "?" +
               qd.urlencode())
        response = self.client.get(url)

        self.assertQuerysetEqual([find_me], response.context["object_list"])
        self.assertNotIn(dont_find_me, response.context["object_list"])

    def test_search_results_keywords_websearch(self) -> None:
        self.client.force_login(self.user)

        find_me = ItemFactory.create(convention=self.convention,
                                     category=self.category,
                                     description="Wombat on toast")

        qd = QueryDict(mutable=True)
        qd.setlist("categories", [self.category.pk])
        qd["convention"] = self.convention.pk
        qd["keywords"] = "wombat on toast"
        qd["search_type"] = "websearch"

        url = (reverse("lost_and_found:results", kwargs={"status": "missing"}) +
               "?" +
               qd.urlencode())
        response = self.client.get(url)

        self.assertQuerysetEqual([find_me], response.context["object_list"])

        qd["keywords"] = "wombat on -toast"
        url = (reverse("lost_and_found:results", kwargs={"status": "missing"}) +
               "?" +
               qd.urlencode())
        response = self.client.get(url)

        self.assertNotIn(find_me, response.context["object_list"])
