# ConOnAPlane

This is the third? fourth? iteration of a long-running project for managing the operations of a convention. It is broadly part of the "ConInABox" project initiated by Thomas Keeley, but its most direct ancestor is [ConOnRails](https://gitlab.com/ConOnRails/ConOnRails), a mature, Rails-based implementation of the concept, which see.

This implementation is in Python, specifically in Django. Lacking a good play on words directly involving either Monty Python or a snake, I went with the more indirect play on words, which also gives me as good an excuse as any to swear like Samuel L. Jackson. It's a terrible name, and I don't deny it, but Naming Things Is Hard.

The motivation to reimplement was simple: Ruby's a great language, Rails an amazing framework, but both are surprisingly hard to find other coders for. Python, on the other hand, has become quite prevalent in a wide variety of fields. It was felt that the bar-to-entry for other volunteers would be lower if they only needed to learn a framework, and not both a language **and** a framework. Time will tell if this proves accurate.

The visual design will borrow heavily from the work DeNae Leverentz did on ConOnRails, with some long-intended updates to take better advantage both of wider screens (when ConOnRails was written, 4:3 monitors were still common!) and much smaller ones (phones).

The internal design, however, is intended to be fully Djangoish, including breaking up the various areas covered by ConOnRails into separate apps, and leveraging the built-in admin and permissions system as much as possible, rather than inventing those wheels.

At this stage, this project is not useful for **anything**. That's how early it is. Stay tuned, fans!

## Requirements

* Python 3.11.1 or later
* PostgreSQL 14 or later

## Development

Development will be much easier if you use the provided docker-compose.yml to fire up a docker-based environment. This will set up the database, the environment variables, and so on. To save your fingers a bit, there is a `bin/dc` utility.

```bash
$ ./bin/dc build
$ ./bin/reseed  # This will tear down the dockerverse, bring it back up, and run the database migration and seeding scripts
```

After that, you should be able to just change code and see those changes reflected when you visit the web page at port 8000.

If you need to run a Django management command, use `bin/manage` to run it in Docker-land.

To run tests, run `bin/test`

## Deployment

COAP configures its connections to other services, along with a few other things, via environment variables.

If running in a containerized environment, these should be provided however your container-orchestration system (Docker Compose, Docker Swarm, Kubernetes, Heroku) does it. The development `docker-compose.yml` shows a simple example with the POSTGRES keys for Docker Compose/Swarm.

If you're running in a "traditional" hosting environment, you will need to copy `.env.example` to `.env` and fill in the values.

**On no account should secrets be checked into a repository. On no account should secrets be hand-edited into `settings.py`. Just don't.**

You will also need to make sure your deployment process does the following things:

### First time

* Set up a virtual environment (`python3 -m venv venv`)

### Every time

* Activate that virtual environment (`source ./venv/bin/activate`)
* Install/update dependencies (`pip3 install -r requirements.txt`)
* Update the view of the database (`python3 manage.py migrate`)

### In a staging environment

* Seed/reseed the database (`./bin/seed-no-docker`)

A note about seeding: Django is not as "cool" as Rails is about seeding. Basically, I'm creating a custom management command for each app. In this branch there's just `seed_log`, but there's already a `seed_lfi` in the lost-and-found branch, for example. What there is not is a master 'seed everything' command, and it's not particularly idempotent right now, so reseeding an already seeded database in a traditional environment may not work very well.

### In a production environment

TBD. There will need to be some scripting for initial setup, to create an admin account and some basic structures in the database. It should not include all the rest of the Lorem Ipsum of the staging seeding environment, however.

### Running as a web server

The Django Project provides [an excellent page of resources](https://docs.djangoproject.com/en/4.2/howto/deployment/) for how to deploy Django apps.
