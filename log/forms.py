from typing import Any, Dict, Type

from django.forms import CheckboxSelectMultiple, Form, ModelForm, IntegerField

from .models import Entry, Thread


class ThreadForm(ModelForm):
    class Meta:
        model: Type = Thread
        fields: list = [
            'status',
            'departments',
            'emergency',
            'medical',
            'post_con',
        ]
        widgets: dict = {
            'departments': CheckboxSelectMultiple
        }


class EntryForm(ModelForm):
    class Meta:
        model: Type = Entry
        fields: list = [
            'description',
            'secure_information'
        ]


class JumpForm(Form):
    jump = IntegerField(min_value=1)

    def clean(self) -> Dict[str, Any]:
        cleaned_data = super().clean()
        pk = cleaned_data.get('jump')

        if pk:
            try:
                Thread.objects.get(pk=pk)
            except Thread.DoesNotExist:
                self.add_error('jump', 'No such thread')
        return cleaned_data
