from datetime import date
import random

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group, Permission
from django.core.management.base import BaseCommand
from django.db import transaction

from accounts import factories as account_facts
from base import factories as base_facts
from base.models import user_role
from departments import factories as department_facts
from log import factories as log_facts


class Command(BaseCommand):
    help = "I need somebody"

    def handle(self, *args, **options):
        with transaction.atomic():
            self._load_fixtures()

    def _load_fixtures(self):
        ops = department_facts.DepartmentFactory.create(name="Operations")
        not_ops = department_facts.DepartmentFactory.create(name="Hotel")

        year = date.today().year

        current_convention = base_facts.CurrentConventionFactory.create(name=f"Not Really Convergence {year}",)
        old_convention = base_facts.ConventionFactory.create()

        departments = (ops, not_ops)

        ordinary_perms = [
            "add_thread",
            "view_thread",
            "add_entry",
            "view_entry",
            "view_department"
        ]
        heads_perms = [
            "can_read_secure_information",
            "add_user",
            "change_user",
            "delete_user",
            "view_user",
            "add_group",
            "view_group",
            "change_group",
            "delete_group",
            "add_department",
            "change_department"
        ]
        # Todo: factory
        heads = Group(name="Heads")
        heads.save()
        heads.permissions.set(Permission.objects.filter(codename__in=(ordinary_perms + heads_perms)))
        heads.department_set.set(departments)  # type: ignore

        ordinaries = Group(name="Ops Staff")
        ordinaries.save()
        ordinaries.permissions.set(Permission.objects.filter(codename__in=ordinary_perms))
        ordinaries.department_set.set(departments)  # type: ignore

        admin = account_facts.UserFactory.create(username="admin",
                                                 password=make_password('controlthehorizontal!'),
                                                 is_staff=True, is_superuser=True)
        account_facts.ProfileFactory.create(name="Invader Zim", user=admin)

        head = account_facts.UserFactory.create(username="head",
                                                password=make_password("doomd00m"),
                                                is_staff=True, is_superuser=False)
        head.groups.set([heads])
        account_facts.ProfileFactory.create(name="Brother Andrew", user=head)

        normie = account_facts.UserFactory.create(username="normie",
                                                  password=make_password("odmo0dm0"),
                                                  is_staff=False, is_superuser=False)
        normie.groups.set([ordinaries])
        account_facts.ProfileFactory.create(name="Newbiest Newb", user=normie)

        peon = account_facts.UserFactory.create(username="peon",
                                                password=make_password("moodm00d"),
                                                is_staff=False, is_superuser=False)
        account_facts.ProfileFactory.create(name="Random Volunteer", user=peon)

        users = (admin, head, normie)

        self.create_threads(current_convention, users, departments)
        self.create_threads(old_convention, users, departments)

    def create_threads(self, convention, users, departments) -> None:
        threads = [log_facts.FuzzyThreadFactory.create(convention=convention,
                                                       departments=random.sample(departments,
                                                                                 random.randint(1, 2)))
                   for _ in range(25)]

        for thread in threads:
            [log_facts.EntryFactory.create(user=user, rolename=user_role(user), thread=thread)
             for _ in range(random.randint(1, 15)) for user in [random.choice(users)]]
