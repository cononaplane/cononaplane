import random
from django.utils import timezone
import factory
import factory.django
import factory.fuzzy
import faker

from factory_djoy import CleanModelFactory

from base.factories import ConventionFactory
from departments.factories import DepartmentFactory
from . import models


fake = faker.Faker(('la',))


d20 = [False for _ in range(18)] + [True, True]


class ThreadFactory(CleanModelFactory):
    class Meta:
        model = models.Thread

    convention = factory.SubFactory(ConventionFactory)
    emergency = False
    medical = False
    post_con = False
    status = 'active'

    @factory.post_generation
    def departments(self, create, extracted, **kwargs) -> None:
        if not create:
            return

        if extracted:
            for department in extracted:
                self.departments.add(department)
        else:
            self.departments.add(DepartmentFactory.create())

    @factory.post_generation
    def created(self, create, extracted, **kwargs) -> None:
        if not create:
            return

        if extracted:
            self.created = extracted
        else:
            if self.convention.end_date > timezone.now().date():
                end_date = timezone.now()
            else:
                end_date = self.convention.end_date

            self.created = fake.date_time_between(self.convention.start_date,
                                                  end_date,
                                                  tzinfo=timezone.get_current_timezone())


class FuzzyThreadFactory(ThreadFactory):
    emergency = factory.fuzzy.FuzzyChoice(d20)
    medical = factory.fuzzy.FuzzyChoice(d20)
    post_con = factory.fuzzy.FuzzyChoice(d20)


class EntryFactory(CleanModelFactory):
    class Meta:
        model = models.Entry

    description = factory.fuzzy.FuzzyAttribute(lambda: "\n".join(fake.paragraphs(random.randint(1, 7))))
    secure_information = factory.fuzzy.FuzzyAttribute(lambda: "" if random.randint(1, 20) < 18
                                                      else "\n".join(fake.paragraphs(random.randint(1, 7))))
    @factory.post_generation
    def created(self, create, extracted, **kwargs) -> None:
        if not create:
            return

        if extracted:
            self.created = extracted
        else:
            if self.thread.convention.end_date > timezone.now().date():
                end_date = timezone.now()
            else:
                end_date = self.thread.convention.end_date

            self.created = fake.date_time_between(self.thread.convention.start_date,
                                                  end_date,
                                                  tzinfo=timezone.get_current_timezone())
