from django.conf import settings
from django.db import models
from django.contrib.postgres.indexes import GinIndex

from model_utils import Choices
from model_utils.fields import MonitorField, StatusField
from model_utils.models import TimeStampedModel as TSModel
from simple_history.models import HistoricalRecords

from base.models import Convention
from departments.models import Department


class EntryStatusBase(models.Model):
    STATUS = Choices('active', 'sticky', 'closed')

    class Meta:
        abstract = True


class Thread(TSModel, EntryStatusBase):
    departments = models.ManyToManyField(Department, db_index=True)
    convention = models.ForeignKey(Convention, on_delete=models.CASCADE)

    emergency = models.BooleanField(db_index=True, default=False)
    medical = models.BooleanField(db_index=True, default=False)
    post_con = models.BooleanField(db_index=True, default=False)
    status = StatusField()
    status_changed = MonitorField(monitor='status')  # type: ignore Pylance doesn't like model_utils

    history = HistoricalRecords(m2m_fields=[departments], bases=[EntryStatusBase])

    @property
    def status_icon(self) -> str:
        ret = ''
        if self.medical:
            ret = 'plus-circle-fill'
        elif self.emergency:
            ret = 'exclamation-octagon-fill'
        elif self.status == 'active':
            ret = 'lightning-fill'
        elif self.status == 'sticky':
            ret = 'sticky-fill'

        return ret

    @property
    def status_for_header(self) -> str:
        ret: str = f"{self.status.capitalize()} "
        for flag in {'emergency', 'medical', 'post_con'}:
            if self.__getattribute__(flag):
                ret += f"{flag.capitalize()} "

        return ret

    @property
    def status_class(self) -> str:
        ret = ''
        if self.medical or self.emergency:
            ret = 'emergency'
        elif self.status == 'active':
            ret = 'is_active'
        elif self.status == 'sticky':
            ret = "sticky"

        return ret

    @property
    def departments_for_header(self) -> str:
        return ', '.join({dept.name for dept in self.departments.all()})

    class Meta:
        ordering = ['-created']


class Entry(TSModel, EntryStatusBase):
    """
    An Entry represents an incident in the log. Entries belong to Threads, which
    hold state. State history is handled by HistoricalRecords and not currently displayed.
    """
    thread = models.ForeignKey(Thread, on_delete=models.DO_NOTHING)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)
    rolename = models.CharField(max_length=25)

    description = models.TextField(help_text="""
                                   Describe the current state of the issue.
                                   Avoid any and all Personally Identifiable Information,
                                   including
                                   <ul>
                                    <li>Names</li>
                                    <li>Badge Names</li>
                                    <li>Badge Numbers</li>
                                   </ul>
                                   """)

    secure_information = models.TextField(help_text="""
                                          Use this field to add any sensitive or
                                          Personally Identifiable Information, health information,
                                          etc. Depending on your role, you may not be
                                          able to see this information once you've entered it.
                                          """,
                                          blank=True)

    class Meta:
        indexes = [
            GinIndex(fields=['description'])
        ]
        ordering = ['-created']
        permissions = [('can_read_secure_information', 'Can read secure information')]
