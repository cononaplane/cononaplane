from django.urls import path

from . import views

app_name = 'log'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index', kwargs={"status": "active"}),
    path('<str:status>', views.IndexView.as_view(), name='index'),
    path('threads', views.ThreadsView.as_view(), name='threads', kwargs={"status": "active"}),
    path('threads/<int:pk>', views.ThreadDetailView.as_view(), name="show"),
    path('threads/<str:status>', views.ThreadsView.as_view(), name='threads'),
    path('entries/new', views.ThreadFormView.as_view(), name='create'),
    path('entries/<int:pk>/update', views.ThreadFormView.as_view(), name='update')
]
