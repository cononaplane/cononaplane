from typing import Any, Dict, List, Set, Type

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db import models, transaction
from django.http import HttpRequest, HttpResponse, QueryDict
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView
from base.models import Convention

from departments.models import Department
from .forms import EntryForm, JumpForm, ThreadForm
from .models import Entry, EntryStatusBase, Thread


class BaseThreadsView(ListView):
    model: Type = Thread
    active_url: str = reverse_lazy("log:threads")
    paginate_by: int = 10
    prefetcher = models.Prefetch('entry_set', queryset=Entry.objects.order_by('-created'))

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        if "jump" in request.GET:
            return redirect(reverse_lazy('log:show', args=(request.GET['jump'],)))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        switch_to = "sticky" if self.status != "sticky" else "active"
        context:  Dict[str, Any] = super().get_context_data(**kwargs)
        context["refresh_url"] = f"{reverse_lazy('log:threads', args=(self.status,))}?page={self.page}"
        context["switcher_url"] = reverse_lazy("log:index", args=(switch_to,))
        context["switch_to"] = switch_to
        context['jump_form'] = JumpForm()
        context['possible_departments'] = Department.objects.filter(pk__in=Department.for_user(self.request.user))
        context['selected_departments'] = self.selected_departments
        context['possible_cons'] = Convention.objects.order_by("-start_date")
        context['selected_convention'] = self.selected_convention.pk
        if self.request.GET.get('keywords'):
            context['keywords'] = self.request.GET.get('keywords')
        context['include_closed'] = self.request.GET.get('include-closed')
        return context

    def get_queryset(self) -> models.QuerySet:
        q = Thread.objects.filter(convention=self.selected_convention,
                                  departments__in=self.selected_departments,
                                  departments__has_log=True)
        if self.request.GET.get('include-closed'):
            q = q.filter(status__in=(self.status, 'closed'))
        else:
            q = q.filter(status=self.status)

        if self.request.GET.get('keywords'):
            q = q.filter(entry__description__search=self.request.GET.get('keywords'))
        return q.prefetch_related(self.prefetcher) \
                .distinct() \
                .order_by('-entry__created')

    # String interpolation can't be done lazy -- it will be done before Django knows its
    # URLs if we define this as a class-level variable like we're supposed to. Fortunately,
    # we can be tricky....
    @property
    def login_url(self):
        return f"{reverse_lazy('login')}?={reverse_lazy('log:threads')}"

    @property
    def status(self) -> str:
        if self.kwargs["status"] in EntryStatusBase.STATUS:
            return self.kwargs["status"]

        return "active"

    @property
    def selected_departments(self) -> Set[int] | None:
        if 'selected_departments' not in self.request.session:
            return Department.for_user(self.request.user)  # [dept.id for dept in self.request.user.d.all()]
        return {int(x) for x in self.request.session['selected_departments']}

    @selected_departments.setter
    def selected_departments(self, value: List[int | str] | Set[int | str] = set()) -> Set[int | str] | None:
        self.request.session['selected_departments'] = value
        return set(value)

    @property
    def selected_convention(self) -> Convention:
        if 'selected_convention' not in self.request.session:
            return Convention.current
        else:
            return Convention.objects.get(pk=int(self.request.session['selected_convention']))

    @selected_convention.setter
    def selected_convention(self, value: int) -> int:
        self.request.session['selected_convention'] = value
        return value

    @property
    def page(self) -> int:
        return int(self.request.GET.get('page') or 1)


class IndexView(PermissionRequiredMixin, BaseThreadsView):
    template_name: str = 'log/index.html'
    permission_required = ("log.view_thread",)
    paginate_by: int = 10

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        if 'dept-select' in request.POST:
            depts: List = request.POST.getlist('dept-select')
            self.selected_departments = [int(x) for x in depts]

        if 'convention' in request.POST:
            con: int = int(request.POST.get('convention') or '0')
            self.selected_convention = con

        redir = reverse_lazy("log:index")
        query = dict()
        if 'keywords' in request.POST and request.POST.get('keywords') and request.POST.get('keywords') != 'None':
            query['keywords'] = request.POST.get('keywords')
        if 'include-closed' in request.POST:
            query['include-closed'] = 'on'
        qd = QueryDict(mutable=True)
        qd.update(query)
        return redirect(redir + "?" + qd.urlencode())


class ThreadsView(PermissionRequiredMixin, BaseThreadsView):
    template_name: str = 'log/_threads.html'
    permission_required = ("log.view_thread",)
    paginate_by: int = 10


class ThreadDetailView(PermissionRequiredMixin, DetailView):
    model: Type = Thread
    prefetcher = models.Prefetch('entry_set', queryset=Entry.objects.order_by('-created'))
    template_name: str = "log/thread.html"
    permission_required = ("log.view_thread",)

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context: Dict[str, Any] = super().get_context_data(**kwargs)
        context['detail'] = True
        return context


class ThreadFormView(PermissionRequiredMixin, TemplateView):
    template_name: str = "log/entry_form.html"
    permission_required = ("log.view_thread", "log.add_thread", "log.view_entry", "log.add_entry")

    def setup(self, request, *args, **kwargs) -> None:
        self.thread: Thread | None = Thread.objects.get(pk=kwargs['pk']) \
            if 'pk' in kwargs else None
        super().setup(request, *args, **kwargs)

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context: Dict[str, Any] = super().get_context_data(**kwargs)
        context['thread_form'] = ThreadForm(prefix='flags', instance=self.thread)
        context['form'] = EntryForm(prefix='entry')
        context['no_update_button'] = self.thread is not None

        if self.thread:
            context['thread'] = self.thread

        return context

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        with transaction.atomic():
            thread_form = ThreadForm(request.POST, prefix='flags', instance=self.thread)
            entry_form = EntryForm(request.POST, prefix='entry')

            if thread_form.is_valid() and entry_form.is_valid():
                if not thread_form.instance.convention_id:
                    thread_form.instance.convention = Convention.current
                thread_form.save()
                entry_form.instance.user = self.request.user
                entry_form.instance.rolename = 'nothing yet'
                entry_form.instance.thread = thread_form.instance
                entry_form.save()
                return redirect(to=self.success_url(thread_form.instance.status))
            else:
                return render(request, self.template_name,
                              {'form': entry_form, 'thread_form': thread_form})

    def success_url(self, status) -> str:
        if status not in ('active', 'sticky'):
            status = 'active'

        return reverse_lazy("log:index", args=(status, ))
