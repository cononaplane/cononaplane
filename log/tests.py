
import unittest

from abc import abstractmethod
from django.test import TransactionTestCase
from django.urls import reverse, reverse_lazy
import faker

from accounts import factories as accounts
from base.factories import ConventionFactory, CurrentConventionFactory
from departments import factories as departments

from . import factories

fake = faker.Faker()


# Flake8 hates this base class. Possibly there's a smarter way to do this, but I haven't
# figured it out yet. The problem is that if it actually derives from TestCase, pytest tries
# to run it independently!
class BaseTestAuthenticated(TransactionTestCase):
    url = ""
    unauth_redirect = ""
    post_redirect = ""
    auth_render = None
    is_form = False

    @classmethod
    def setUpClass(cls) -> None:
        if cls is BaseTestAuthenticated:
            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")

        super(BaseTestAuthenticated, cls).setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        return super().tearDownClass()

    def setUp(self):
        super().setUp()
        self.user = accounts.UserFactory.create()
        self.peon_user = accounts.UserFactory.create()
        self.group = accounts.HeadGroupFactory.create()
        self.convention = CurrentConventionFactory.create()
        self.user.groups.add(self.group)

    def test_get_unauthenticated(self) -> None:
        response = self.client.get(self.url)
        self.assertRedirects(response, self.unauth_redirect)

    def test_get_authenticated(self) -> None:
        self.client.force_login(self.user)  # type: ignore
        response = self.client.get(self.url)
        self.assertTemplateUsed(response, self.auth_render)

        self.do_more_get_authenticated(response)

    def test_get_authenticated_but_no_perms(self) -> None:
        self.client.force_login(self.peon_user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_post_authenticated(self) -> None:
        """
        Test the happy path of an authenticated POST

        This should always test a happy path, with get_form_data() returnning what
        a valid form would post. Individual test case classes will need to handle
        complexity in forms, because every form is different
        """
        if not self.is_form:
            return
        self.client.force_login(self.user)  # type: ignore
        form_data = self.get_form_data()
        response = self.client.post(self.url, data=form_data)
        self.assertRedirects(response, self.post_redirect)

    def test_post_authenticated_but_no_perms(self) -> None:
        if not self.is_form:
            return
        self.client.force_login(self.peon_user)  # type: ignore
        form_data = self.get_form_data()
        response = self.client.post(self.url, data=form_data)
        self.assertEqual(response.status_code, 403)

    @abstractmethod
    def do_more_get_authenticated(self, response):
        pass

    @abstractmethod
    def get_form_data(self) -> dict:
        pass


class TestThreadsView(BaseTestAuthenticated):
    url = reverse("log:index", args=("active",))
    unauth_redirect = f"{reverse('login')}?next={url}"
    auth_render = 'log/_threads.html'

    def setUp(self) -> None:
        super().setUp()
        self.department = departments.DepartmentFactory.create()
        self.other_department = departments.DepartmentFactory.create()
        self.department.users.add(self.user)

        self.active_thread_can_see = factories.ThreadFactory.create(status='active',
                                                                    convention=self.convention,
                                                                    departments=[self.department])
        self.active_thread_also_can_see = factories.ThreadFactory.create(status='active',
                                                                         convention=self.convention,
                                                                         departments=[self.department,
                                                                                      self.other_department])
        self.active_thread_cannot_see = factories.ThreadFactory.create(status='active',
                                                                       convention=self.convention,
                                                                       departments=[self.other_department])

    def do_more_get_authenticated(self, response):
        self.assertNotIn(self.active_thread_cannot_see, response.context['object_list'])
        self.assertIn(self.active_thread_can_see, response.context['object_list'])
        self.assertIn(self.active_thread_also_can_see, response.context['object_list'])


class TestStickyIndexView(BaseTestAuthenticated):
    url = reverse("log:index", args=("sticky", ))
    unauth_redirect = f"{reverse('login')}?next={url}"
    auth_render = 'log/index.html'

    def setUp(self) -> None:
        super().setUp()
        self.department = departments.DepartmentFactory.create()
        self.department.users.add(self.user)
        self.department.save()
        self.sticky_thread = factories.ThreadFactory.create(status='sticky',
                                                            convention=self.convention,
                                                            departments=[self.department])
        self.active_thread = factories.ThreadFactory.create(status='active',
                                                            convention=self.convention,
                                                            departments=[self.department])

    def do_more_get_authenticated(self, response) -> None:
        self.assertNotIn(self.active_thread, response.context['object_list'])
        self.assertIn(self.sticky_thread, response.context['object_list'])


class TestStickyThreadsView(TestStickyIndexView):
    url = reverse("log:threads", args=("sticky", ))
    unauth_redirect = f"{reverse('login')}?next={url}"
    auth_render = 'log/_threads.html'


class TestThreadFormViewOnCreate(BaseTestAuthenticated):
    url = reverse("log:create")
    unauth_redirect = f"{reverse('login')}?next={url}"
    post_redirect = reverse_lazy('log:index', args=("active",))
    auth_render = "log/entry_form.html"
    is_form = True

    def setUp(self) -> None:
        super().setUp()
        self.department = departments.DepartmentFactory.create()

    def do_more_get_authenticated(self, response) -> None:
        self.assertIsNotNone(response.context['thread_form'])
        self.assertIsNotNone(response.context['form'])
        self.assertFalse(response.context['no_update_button'])
        self.assertNotIn("thread", response.context)

    def get_form_data(self) -> dict:
        return {
            "entry-description": [fake.paragraph()],
            "flags-departments": [self.department.pk],
            "flags-status": "active"

        }


class TestThreadFormViewOnUpdate(BaseTestAuthenticated):
    post_redirect = reverse_lazy('log:index', args=("active",))
    auth_render = "log/entry_form.html"
    is_form = True

    def setUp(self) -> None:
        super().setUp()
        self.department = departments.DepartmentFactory.create()
        self.thread = factories.ThreadFactory.create(convention=self.convention,
                                                     departments=(self.department,),
                                                     status='active')

    def get_form_data(self) -> dict:
        return {
            "entry-description": [fake.paragraph()],
            "flags-departments": [self.department.pk],
            "flags-status": "active"

        }

    def test_flags_change(self) -> None:
        other_dept = departments.DepartmentFactory.create()
        forms_data = {
            "entry-description": [fake.paragraph()],
            "flags-departments": [self.department.pk, other_dept.pk],
            "flags-status": "active"
        }

        self.client.force_login(self.user)  # type: ignore
        self.assertEqual(len(self.thread.departments.all()), 1)
        response = self.client.post(self.url, data=forms_data)
        self.assertRedirects(response, self.post_redirect)
        self.assertEqual(len(self.thread.departments.all()), 2)

        forms_data = {
            "entry-description": [fake.paragraph()],
            "flags-departments": [self.department.pk, other_dept.pk],
            "flags-status": ["active"],
            "flags-emergency": ["on"],
        }

        response = self.client.post(self.url, data=forms_data)
        self.assertRedirects(response, self.post_redirect)
        self.thread.refresh_from_db()
        self.assertTrue(self.thread.emergency)

    def test_status_change(self) -> None:
        forms_data = {
            "entry-description": [fake.paragraph()],
            "flags-departments": [self.department.pk],
            "flags-status": ["sticky"],
        }

        self.client.force_login(self.user)  # type: ignore
        response = self.client.post(self.url, data=forms_data)
        self.assertRedirects(response, reverse("log:index", args=("sticky",)))

    @property
    def url(self) -> str:
        return reverse("log:update", args=(self.thread.pk,))

    @property
    def unauth_redirect(self) -> str:
        return f"{reverse('login')}?next={self.url}"
