from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from accounts.models import Profile, Training


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    max_num = 1
    verbase_name_plural = "profile"


class UserAdmin(BaseUserAdmin):
    inlines = (ProfileInline,)
    fieldsets = BaseUserAdmin.fieldsets
    list_display = ('username', 'email', 'profile_name')

    for key, value in fieldsets:
        # Hide fields we're not using
        if key == 'Personal info':
            value["fields"] = ['email', ]

    @admin.display(description="Name")
    def profile_name(self, obj):
        return obj.profile.name


@admin.register(Training)
class TrainingAdmin(admin.ModelAdmin):
    list_filter = ('users',)
    filter_horizontal = ('users',)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
