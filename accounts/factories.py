from django.contrib.auth.models import User, Group, Permission
import factory
import factory.django
import factory.fuzzy
from faker import Faker
from faker.providers import internet, misc
from .models import Profile

fake = Faker()
fake.add_provider(internet)
fake.add_provider(misc)


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.fuzzy.FuzzyAttribute(fake.user_name)
    email = factory.fuzzy.FuzzyAttribute(fake.email)


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Group

    name = factory.fuzzy.FuzzyAttribute(fake.user_name)


class HeadGroupFactory(GroupFactory):

    @factory.post_generation
    def permissions(self, create, extracted, **kwargs):
        heads_perms = [
            'can_read_secure_information',
            'add_thread',
            'view_thread',
            'add_entry',
            'view_entry',
            'add_user',
            'change_user',
            'delete_user',
            'view_user',
            'add_group',
            'view_group',
            'change_group',
            'delete_group',
            'add_department',
            'change_department',
            'view_department',
            'view_item'
        ]

        if not create:
            return

        perms = Permission.objects.filter(codename__in=heads_perms)
        self.permissions.set(perms)


class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile

    name = factory.fuzzy.FuzzyAttribute(fake.name)
    user = factory.SubFactory(UserFactory)
