# Accounts

Django provides a lot of account magic as part of the authentication app that ships with it, but we need to do a few things ourselves. For example,
if we want user signup, that's on us (although the app does provide a basic form for it).

This app is for the paths the app doesn't already cover, and takes this [Django sign-up tutorial](https://learndjango.com/tutorials/django-signup-tutorial) as its inspiration.
