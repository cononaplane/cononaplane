from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(to=User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, db_index=True)


class Training(models.Model):
    name = models.CharField(max_length=20, unique=True)
    users = models.ManyToManyField(to=User, blank=True)

    def __str__(self) -> str:
        return self.name
