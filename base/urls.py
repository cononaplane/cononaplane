from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('banner', views.BannerView.as_view(), name='banner')
]
