from datetime import date, datetime

import factory
import factory.django
import factory.fuzzy

from factory_djoy import CleanModelFactory

import faker

from .models import Convention


fake = faker.Faker()


class ConventionFactory(CleanModelFactory):
    class Meta:
        model = Convention

    name = factory.fuzzy.FuzzyAttribute(fuzzer=fake.name)
    start_date = date(1970, 1, 1)
    end_date = date(1971, 1, 1)


class CurrentConventionFactory(ConventionFactory):
    start_date = date(datetime.now().year, 1, 1)
    end_date = date(datetime.now().year + 1, 1, 1)
