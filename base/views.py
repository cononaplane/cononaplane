from typing import Any, Dict
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from log.models import Thread


@login_required
def index(request: HttpRequest) -> HttpResponse:
    response = render(request, 'base/index.html')
    return response


class BannerView(LoginRequiredMixin, TemplateView):
    template_name = 'base/_banner.html'

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context: Dict[str, Any] = super().get_context_data(**kwargs)
        context['active_count'] = Thread.objects.filter(status='active').count()
        context['emergency_count'] = Thread.objects.filter(status='active', emergency=True).count()
        context['medical_count'] = Thread.objects.filter(status='active', medical=True).count()

        context['background_class'] = self.__get_background_class(context)
        return context

    def get(self, request, *args, **kwargs) -> HttpResponse:
        return super().get(request, args, kwargs)

    def __get_background_class(self, context: dict) -> str:
        """
        Priority is:
            * Emergency or medical: class is emergency
            * Active issues: class is is_active (avoiding collision with Bootstrap)
            * Else: no class (default)
        """
        if context['emergency_count'] > 0 or \
           context['medical_count'] > 0:
            return 'emergency'
        elif context['active_count'] > 0:
            return 'is_active'
        else:
            return ''
