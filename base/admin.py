from django.contrib import admin

from .models import Convention

@admin.register(Convention)
class ConventionAdmin(admin.ModelAdmin):
    pass
