"""
Models for the base app
"""
from datetime import date
from django.db import models
from django.forms import ValidationError


def user_role(user) -> str:
    """
    Return the user's current role. Not fully implemented, yet.
    Current:
    * If admin, say admin
    * If not a member of any group, return <no role>
    * Otherwise, return the first group on the list

    Intended:
    * Allow a user to specify at login what role they're fulfilling
      and store it in the session, refer to it here.
    """
    if user.is_superuser:
        return "admin"
    if user.groups.count() == 0:
        return "<no role>"

    return user.groups.all()[0].name


def date_range_validator(start: date, end: date) -> None:
    """
    No overlaps, no inclusions.
    """
    # end1 ≥ start2 and end2 ≥ start1
    query = Convention.objects.filter(end_date__gt=start, start_date__lte=end)
    if query.count() > 0:
        raise ValidationError(f"These dates {start}->{end} overlap with an existing"
                              f"convention {query}")


class Convention(models.Model):
    """
    Model representing a convention period.
    """
    name = models.CharField(max_length=100, db_index=True, unique=True)
    start_date = models.DateField(blank=False, db_index=True,
                                  help_text="""
                                  Inclusive--this should be the first day of the convention year.
                                  It may also be the same day as the previous year's end date.
                                  """)
    end_date = models.DateField(blank=False, db_index=True,
                                help_text="""
                                Exclusive--this should be the day AFTER the last
                                date of the convention year. It may also be the same day as
                                the next year's start date.
                                """)

    @classmethod
    @property
    def current(cls):
        """
        Return the current convention. If nothing matches the date, return
        the most recently held convention. If we haven't defined any conventions
        yet, return None
        """
        now = date.today()
        ret = None
        try:
            ret = Convention.objects.get(start_date__lte=now, end_date__gt=now)
        except cls.DoesNotExist:
            q = Convention.objects.filter(start_date__lte=now).order_by("-start_date")
            if len(q) > 0:
                ret = q[0]
            else:
                ret = None
        return ret

    def clean(self) -> None:
        date_range_validator(self.start_date, self.end_date)
        return super().clean()

    def __str__(self) -> str:
        return f"{self.name}: {self.start_date} -> {self.end_date}"
