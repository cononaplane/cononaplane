"""
Base tests
"""
from datetime import date, timedelta

from django.forms import ValidationError
from django.test import TestCase
from django.urls import reverse

from accounts import factories
from base.factories import ConventionFactory, CurrentConventionFactory
from log.factories import ThreadFactory

from .models import Convention
from .views import BannerView


class TestBase(TestCase):
    """
    Base class for tests
    """
    def setUp(self):
        self.user = factories.UserFactory()

    def test_index_unauthenticated(self) -> None:
        response = self.client.get(reverse('index'))
        self.assertRedirects(response, '/accounts/login/?next=/')

    def test_index_authenticated(self) -> None:
        self.client.force_login(self.user)
        response = self.client.get(reverse('index'))
        self.assertTemplateUsed(response, 'base/index.html')

    def test_logout(self) -> None:
        response = self.client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/logged_out.html')


class TestBannerView(TestCase):
    """
    Test banner view
    """
    def setUp(self):
        self.view = BannerView()
        self.convention = CurrentConventionFactory.create()

    def test_get_context_data(self):
        # No Threads
        ctx = self.view.get_context_data()
        self.assertEqual(ctx['active_count'], 0)
        self.assertEqual(ctx['emergency_count'], 0)
        self.assertEqual(ctx['medical_count'], 0)
        self.assertEqual(ctx['background_class'], '')

        # One active thread
        active = ThreadFactory.create(status='active', convention=self.convention)
        ctx = self.view.get_context_data()
        self.assertEqual(ctx['active_count'], 1)
        self.assertEqual(ctx['emergency_count'], 0)
        self.assertEqual(ctx['medical_count'], 0)
        self.assertEqual(ctx['background_class'], 'is_active')

        # Add one active emergency
        emergency = ThreadFactory.create(status='active', convention=self.convention, emergency=True)
        ctx = self.view.get_context_data()
        self.assertEqual(ctx['active_count'], 2)
        self.assertEqual(ctx['emergency_count'], 1)
        self.assertEqual(ctx['medical_count'], 0)
        self.assertEqual(ctx['background_class'], 'emergency')

        # Add one active medical
        medical = ThreadFactory.create(status='active', convention=self.convention, medical=True)
        ctx = self.view.get_context_data()
        self.assertEqual(ctx['active_count'], 3)
        self.assertEqual(ctx['emergency_count'], 1)
        self.assertEqual(ctx['medical_count'], 1)
        self.assertEqual(ctx['background_class'], 'emergency')

        #  Add one closed emergency
        ThreadFactory.create(status='closed', convention=self.convention, emergency=True)
        ctx = self.view.get_context_data()

        # Close the medical
        medical.status = 'closed'
        medical.save()
        ctx = self.view.get_context_data()
        self.assertEqual(ctx['active_count'], 2)
        self.assertEqual(ctx['emergency_count'], 1)
        self.assertEqual(ctx['medical_count'], 0)
        self.assertEqual(ctx['background_class'], 'emergency')

        # Close the emergency
        emergency.status = 'closed'
        emergency.save()
        ctx = self.view.get_context_data()
        self.assertEqual(ctx['active_count'], 1)
        self.assertEqual(ctx['emergency_count'], 0)
        self.assertEqual(ctx['medical_count'], 0)
        self.assertEqual(ctx['background_class'], 'is_active')

        # Close the last issue
        active.status = 'closed'
        active.save()
        ctx = self.view.get_context_data()
        self.assertEqual(ctx['active_count'], 0)
        self.assertEqual(ctx['emergency_count'], 0)
        self.assertEqual(ctx['medical_count'], 0)
        self.assertEqual(ctx['background_class'], '')


class TestConventionValidation(TestCase):
    """
    Test convention validation
    """
    def setUp(self):
        super().setUp()
        self.existing_con = Convention(name="Foo", start_date=date(1970, 1, 1), end_date=date(1971, 1, 1))
        self.existing_con.save()

    def tearDown(self) -> None:
        self.existing_con.delete()
        return super().tearDown()

    def test_no_overlap(self):
        new_con = ConventionFactory.build(name="Bar", start_date=date(1971, 1, 1), end_date=date(1972, 1, 1))
        new_con.full_clean()

    def test_reject_exact_overlap(self) -> None:
        exact_overlap = ConventionFactory.build(name="Bar", start_date=self.existing_con.start_date,
                                                end_date=self.existing_con.end_date)
        with self.assertRaises(ValidationError):
            exact_overlap.full_clean()

    def test_reject_inner_overlap(self) -> None:
        inner_overlap = ConventionFactory.build(name="Baz",
                                                start_date=self.existing_con.start_date + timedelta(days=5),
                                                end_date=self.existing_con.end_date - timedelta(days=5))
        with self.assertRaises(ValidationError):
            inner_overlap.full_clean()

    def test_reject_overhang(self) -> None:
        overhang = ConventionFactory.build(name="Qux",
                                          start_date=self.existing_con.start_date + timedelta(days=364),
                                          end_date=self.existing_con.end_date + timedelta(days=365))
        with self.assertRaises(ValidationError):
            overhang.full_clean()

    def test_reject_underbite(self) -> None:
        underbite = ConventionFactory.build(name="Quux",
                                            start_date=self.existing_con.start_date - timedelta(days=2),
                                            end_date=self.existing_con.end_date - timedelta(days=2))

        with self.assertRaises(ValidationError):
            underbite.full_clean()

    def test_reject_superset(self) -> None:
        superset = ConventionFactory.build(name="Quuux",
                                           start_date=self.existing_con.start_date - timedelta(days=5),
                                           end_date=self.existing_con.end_date + timedelta(days=5))
        with self.assertRaises(ValidationError):
            superset.full_clean()


class TestConventionCurrent(TestCase):
    """
    Test ConventionCurrent shortcut class
    """
    def test_when_there_is_no_convention(self) -> None:
        self.assertIsNone(Convention.current)

    def test_when_there_is_a_convention_but_none_are_current(self) -> None:
        """Will return the most recent instead"""
        ConventionFactory.create()
        self.assertIsNotNone(Convention.current)

    def test_when_there_is_a_current_convention(self) -> None:
        CurrentConventionFactory.create()
        self.assertIsNotNone(Convention.current)
