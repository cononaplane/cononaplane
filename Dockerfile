FROM python:3.11-bullseye
EXPOSE 8000
WORKDIR /app

RUN apt update && apt install -y postgresql-client apache2-dev
RUN python -m venv venv
COPY requirements.txt /app
RUN pip install -r requirements.txt --no-cache-dir
COPY . /app
CMD ["python3.11", "manage.py", "runserver", "0.0.0.0:8000"]
