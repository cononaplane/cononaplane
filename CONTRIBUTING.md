This project currently targets the specific needs of the [CONvergence Convention](https://convergence-con.org) Operations Department, but is intended to be more broadly useful. Even within the context of that particular convention, more contributions are definitely welcome.

This project is in its earliest stages, so no solid road map or documents yet exist. They're coming, though.

As per the license, you can fork this any time you like, but please talk to me **first** before sending a pull request, so it doesn't catch me sideways or work at cross purposes.

-- Michael Scott Shappe <mshappe@camelopard-consulting.com>
