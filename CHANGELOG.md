# 2023-02-11 - Michael <mshappe@camelopard-consulting.com>

* Can now switch to viewing stickies
* When changing status to sticky, redirect will be to stickies
* When changing status back to active, redirect will be to active
* When changing status to closed, redirect will be to active.
* Styles updated to include stickies
* Expanded testing a lot

# 2023-02-04 - Michael <mshappe@camelopard-consulting.com>

* Ditched the `events` app for a `log` app. Names have changed a little bit, as has the data design. This is still likely to change more but right now it behaves more or less as intended!

* Introduced HTMX to greatly simplify event and banner refresh without needing to hand-roll javascript.

* Starting to pirate some of the stylesheet out of ConOnRails.

# 2023-01-21 - Michael <mshappe@camelopard-consulting.com>

* First real non-scaffolding push. Doesn't do much of anything but show
a foul-mouthed splashpage. You know the Barenaked Ladies song
"Box / Set"? This the demo recorded in my basement.

* Added basic login/logout

* Added `events` app and began modeling.
